#!/bin/sh

for f in *.ipynb **/*.ipynb; do
    da="$(date -R -r $f)"
    jupyter-nbconvert $f --to notebook --ClearOutputPreprocessor.enabled=True --inplace
    touch -d "$da" $f 
done
