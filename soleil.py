from typing import (Callable, Generic, Iterable, Iterator, List, NamedTuple,
                    NewType, Optional, Text, Tuple, TypeVar, Union)

import os

import numpy
import pyFAI

from collections.abc import Mapping
from dataclasses import dataclass
from functools import partial

from fabio.edfimage import edfimage
from h5py import Dataset, File
from numpy import arange, arctan, array, ndarray, logical_or, ma, rad2deg, where, zeros_like
from numpy.ma import MaskedArray
from pyFAI.goniometer import GeometryTransformation, GoniometerRefinement, ExtendedTransformation, MultiGeometry
from pyFAI.gui import jupyter
from pyFAI.containers import Integrate1dResult

# TODO the label should be part of the calibration frame

# NewTypes

Angle = NewType("Angle", float)
BinSize = NewType("BinSize", Tuple[int, int])
Calibrant = NewType("Calibrant", Text)
Detector = NewType("Detector", Text)
Length = NewType("Length", float)
NumExpr = NewType("NumExpr", Text)
Threshold = NewType("Threshold", int)
Wavelength = NewType("Wavelength", float)

# Typevar

T = TypeVar('T', int, float, Angle, Length)

@dataclass
class Parameter(Generic[T]):
    name: Text
    value: T
    bounds: Tuple[T, T]

# Generic hdf5 access types.

@dataclass
class DatasetPathContains:
    path: Text

@dataclass
class DatasetPathWithAttribute:
    attribute: Text
    value: bytes

DatasetPath = Union[DatasetPathContains,
                    DatasetPathWithAttribute]


def _v_attrs(attribute: Text, value: Text, _name: Text, obj) -> Optional[Dataset]:
    """extract all the images and accumulate them in the acc variable"""
    res = None
    if isinstance(obj, Dataset):
        if attribute in obj.attrs and obj.attrs[attribute] == value:
            res = obj
    return res


def _v_item(key: Text, name: Text, obj: Dataset) -> Optional[Dataset]:
    res = None
    if key in name:
        res = obj
    return res


def get_dataset(h5file: File, path: DatasetPath) -> Optional[Dataset]:
    res = None
    if isinstance(path, DatasetPathContains):
        res = h5file.visititems(partial(_v_item, path.path))
    elif isinstance(path, DatasetPathWithAttribute):
        res = h5file.visititems(partial(_v_attrs,
                                        path.attribute, path.value))
    return res

# Calibration K6C


@dataclass
class CalibrationFrame:
    idx: int
    label: Text
    image: ndarray
    delta: Angle


@dataclass
class CalibrationFunctions:
    distance: NumExpr
    poni1: NumExpr
    poni2: NumExpr
    rot1: NumExpr
    rot2: NumExpr
    rot3: NumExpr


@dataclass
class ExtendedCalibrationFunctions(CalibrationFunctions):
    wavelength: NumExpr


Functions = NewType("Functions",
                    Tuple['ExtendedCalibrationFunctions', List['Parameter']])


@dataclass
class Calibration:
    basedir: Text
    filename: Text
    images_path: DatasetPath
    deltas_path: DatasetPath
    idxs: List[int]
    to_use: Callable[[CalibrationFrame], bool]
    calibrant: Calibrant
    detector: Detector
    bin_size: BinSize
    pixel1: Length
    pixel2: Length
    wavelength: Wavelength
    functions: Functions
    mask: Text


def gen_metadata_idx(h5file: File,
                     calibration: Calibration,
                     indexes: Optional[Iterable[int]]=None, # noqa
                     to_use: Optional[bool]=True) -> Iterator[CalibrationFrame]:    # noqa
    images = get_dataset(h5file, calibration.images_path)
    deltas = get_dataset(h5file, calibration.deltas_path)
    if images is not None and deltas is not None:
        if indexes is None:
            indexes = range(images.shape[0])
        base = os.path.basename(calibration.filename)
        for idx in indexes:
            label = base + "_{:d}".format(idx)
            frame = CalibrationFrame(idx, label, images[idx], deltas[idx])
            if calibration.to_use(frame) is True or to_use is False:
                yield CalibrationFrame(idx, label, images[idx], deltas[idx])


def save_as_edf(calibration: Calibration) -> List[Text]:
    """Save the multi calib images into edf files in order to do the first
    calibration and print the command line in order to do the
    calibration with pyFAI-calib
    """
    cmds = []
    with File(calibration.filename, mode='r') as h5file:
        for frame in gen_metadata_idx(h5file, calibration, calibration.idxs):
            base = os.path.basename(calibration.filename)
            output = base + "_{:02d}.edf".format(frame.idx)
            edfimage(frame.image).write(os.path.join(calibration.basedir, output))  # noqa
            # temporary until pyFAI-calib2 works
            wavelength = calibration.wavelength * 1e10
            npt_fullpath = os.path.join(calibration.basedir, base + f"_{frame.idx:02d}.npt")
            cmd_npt = f"-n {npt_fullpath}" if os.path.exists(npt_fullpath) else ""
            cmd = f"cd {calibration.basedir} && pyFAI-calib2 -w {wavelength} --calibrant {calibration.calibrant} -D {calibration.detector} -m {calibration.mask} {cmd_npt} {output}"
            cmds.append(cmd)
    return cmds


def get_total_length(calibration: Calibration) -> Optional[int]:
    """Return the total number of frame of the calib file"""
    res = None
    with File(calibration.filename, mode='r') as h5file:
        images = get_dataset(h5file, calibration.images_path)
        if images is not None:
            res = images.shape[0]
    return res


def optimize_with_new_images(h5file: File,
                             calibration: Calibration,
                             gonioref,
                             calibrant: pyFAI.calibrant.Calibrant,
                             indexes: Optional[Iterable[int]],
                             pts_per_deg: float=1,
                             dark: ndarray=None) -> None:
    """This function adds new images to the pool of data used for the
    refinement.  A set of new control points are extractred and a
    refinement step is performed at each iteration The last image of
    the serie is displayed

    """
    sg = None
    for frame in gen_metadata_idx(h5file, calibration, indexes):
        print()
        if frame.label in gonioref.single_geometries:
            continue
        print(frame.label)
        if dark is not None:
            image = frame.image - dark
        else:
            image = frame.image
        sg = gonioref.new_geometry(frame.label, image=image,
                                   metadata=frame,
                                   calibrant=calibrant)
        print(sg.extract_cp(pts_per_deg=pts_per_deg))
    print("*"*50)
    gonioref.refine2()
    if sg:
        sg.geometry_refinement.set_param(gonioref.get_ai(sg.get_position()).param)  # noqa
        jupyter.display(sg=sg)


def get_calibrant(calibrant: Calibrant,
                  wavelength: Wavelength) -> pyFAI.calibrant.Calibrant:
    """Return the calibrant with the right wavelength"""
    pyFAI_calibrant = pyFAI.calibrant.get_calibrant(calibrant)
    pyFAI_calibrant.wavelength = wavelength
    return pyFAI_calibrant


def get_detector(detector: Detector, bin_size: BinSize=BinSize((1, 1)), pixel1: Optional[Length]=None, pixel2: Optional[Length]=None) -> pyFAI.detectors.Detector:
    det = pyFAI.detector_factory(detector)
    det.set_binning(bin_size)
    if pixel1 is not None:
        det.set_pixel1(pixel1)
    if pixel2 is not None:
        det.set_pixel2(pixel2)
    return det


def get_poni(params: Calibration, idx: int) -> Text:
    base = os.path.basename(params.filename)
    return os.path.join(params.basedir, base + "_{:02d}.poni".format(idx))


def calibration(json: str,
                params: Calibration,
                indexes: Optional[Iterable[int]]=None,
                fit_wavelength: bool=False,
                dark: ndarray=None) -> None:
    """Do a calibration with a bunch of images"""

    # Definition of the geometry refinement: the parameter order is
    # the same as the param_names
    calibrant = get_calibrant(params.calibrant,
                              params.wavelength)
    detector = get_detector(params.detector, params.bin_size,
                            params.pixel1, params.pixel2)

    (functions, initial_parameters) = params.functions

    # Let's refine poni1 and poni2 also as function of the distance:

    if fit_wavelength == True:
        parameters = {p.name: p.value for p in initial_parameters}
        bounds = {p.name: p.bounds for p in initial_parameters}
        param_names = [p.name for p in initial_parameters]
        trans_function = ExtendedTransformation(param_names=param_names,
                                                pos_names=["delta"],
                                                dist_expr=functions.distance,
                                                poni1_expr=functions.poni1,
                                                poni2_expr=functions.poni2,
                                                rot1_expr=functions.rot1,
                                                rot2_expr=functions.rot2,
                                                rot3_expr=functions.rot3,
                                                wavelength_expr=functions.wavelength)
    else:
        parameters = {p.name: p.value for p in initial_parameters if p.name != 'energy'}
        bounds = {p.name: p.bounds for p in initial_parameters if p.name != 'energy'}
        param_names = [p.name for p in initial_parameters if p.name != 'energy']
        trans_function = GeometryTransformation(param_names=param_names,
                                                pos_names=["delta"],
                                                dist_expr=functions.distance,
                                                poni1_expr=functions.poni1,
                                                poni2_expr=functions.poni2,
                                                rot1_expr=functions.rot1,
                                                rot2_expr=functions.rot2,
                                                rot3_expr=functions.rot3)

    def pos_function(frame: CalibrationFrame) -> Tuple[float]:
        """Definition of the function reading the detector position from the
        header of the image."""
        return (frame.delta,)

    gonioref = GoniometerRefinement(parameters,  # initial guess
                                    bounds=bounds,
                                    pos_function=pos_function,
                                    trans_function=trans_function,
                                    detector=detector,
                                    wavelength=params.wavelength)

    print("Empty refinement object:")
    print(gonioref)

    # Let's populate the goniometer refinement object with the known poni

    with File(params.filename, mode='r') as h5file:
        for frame in gen_metadata_idx(h5file, params, params.idxs):
            base = os.path.basename(params.filename)
            control_points = os.path.join(params.basedir, base + "_{:02d}.npt".format(frame.idx))  # noqa
            ai = None
            try:
                ai = pyFAI.load(os.path.join(params.basedir, base + "_{:02d}.poni".format(frame.idx)))  # noqa
            except FileNotFoundError:
                pass

            gonioref.new_geometry(frame.label, frame.image, frame,
                                  control_points, calibrant, geometry=ai)

        print("Filled refinement object:")
        print(gonioref)
        print(os.linesep + "\tlabel \t tx")
        for k, v in gonioref.single_geometries.items():
            print(k, v.get_position())

        for g in gonioref.single_geometries.values():
            ai = gonioref.get_ai(g.get_position())
            print(ai)

        for sg in gonioref.single_geometries.values():
            jupyter.display(sg=sg)

        gonioref.refine2()

    for multi in [params]:
        with File(multi.filename, mode='r') as h5file:
            optimize_with_new_images(h5file, multi, gonioref,
                                     calibrant, indexes, pts_per_deg=10,
                                     dark=dark)

    #gonioref.refine2(method="simplex")
    
    for idx, sg in enumerate(gonioref.single_geometries.values()):
        sg.geometry_refinement.set_param(gonioref.get_ai(sg.get_position()).param)  # noqa
        jupyter.display(sg=sg)

    gonioref.save(json)

# Integrate

def integrate1d(self,
                lst_data,
                npt=1800,
                correctSolidAngle=True,
                lst_variance=None,
                error_model=None,
                polarization_factor=None,
                normalization_factor=None,
                all=False,
                lst_mask=None,
                lst_flat=None):
    """Perform 1D azimuthal integration
    :param lst_data: list of numpy array
    :param npt: number of points int the integration
    :param correctSolidAngle: correct for solid angle (all processing are then done in absolute solid angle !)
    :param lst_variance: list of array containing the variance of the data. If not available, no error propagation is done
    :type lst_variance: list of ndarray
    :param error_model: When the variance is unknown, an error model can be given: "poisson" (variance = I), "azimuthal" (variance = (I-<I>)^2)
    :type error_model: str
    :param polarization_factor: Apply polarization correction ? is None: not applies. Else provide a value from -1 to +1
    :param normalization_factor: normalization monitors value (list of floats)
    :param all: return a dict with all information in it (deprecated, please refer to the documentation of Integrate1dResult).
    :param lst_mask: numpy.Array or list of numpy.array which mask the lst_data.
    :param lst_flat: numpy.Array or list of numpy.array which flat the lst_data.
    :return: 2th/I or a dict with everything depending on "all"
    :rtype: Integrate1dResult, dict
    """
    if len(lst_data) == 0:
        raise RuntimeError("List of images cannot be empty")
    if normalization_factor is None:
        normalization_factor = [1.0] * len(self.ais)
    elif not isinstance(normalization_factor, collections.Iterable):
        normalization_factor = [normalization_factor] * len(self.ais)
    if lst_variance is None:
        lst_variance = [None] * len(self.ais)
    if lst_mask is None:
        lst_mask = [None] * len(self.ais)
    elif isinstance(lst_mask, numpy.ndarray):
        lst_mask = [lst_mask] * len(self.ais)
    if lst_flat is None:
        lst_flat = [None] * len(self.ais)
    elif isinstance(lst_flat, numpy.ndarray):
        lst_flat = [lst_flat] * len(self.ais)
    sum_ = numpy.zeros(npt, dtype=numpy.float64)
    count = numpy.zeros(npt, dtype=numpy.float64)
    sigma2 = None
    results = []
    for ai, data, monitor, variance, mask, flat in zip(self.ais, lst_data, normalization_factor, lst_variance, lst_mask, lst_flat):
        res = ai.integrate1d(data, npt=npt,
                             correctSolidAngle=correctSolidAngle,
                             variance=variance, error_model=error_model,
                             polarization_factor=polarization_factor,
                             radial_range=(0, 80),  # self.radial_range,
                             azimuth_range=self.azimuth_range,
                             method="splitpixel", unit=self.unit, safe=True,
                             mask=mask, flat=flat)
        results.append(res)
        sac = (ai.pixel1 * ai.pixel2 / ai.dist ** 2) if correctSolidAngle else 1.0
        count += res.count * sac
        sum_ += res.sum / monitor
        if res.sigma is not None:
            if sigma2 is None:
                sigma2 = numpy.zeros(npt, dtype=numpy.float64)
            sigma2 += (res.sigma ** 2) / monitor

    tiny = numpy.finfo("float32").tiny
    norm = numpy.maximum(count, tiny)
    invalid = count <= 0.0
    I = sum_ / norm
    I[invalid] = self.empty

    if sigma2 is not None:
        sigma = numpy.sqrt(sigma2) / norm
        sigma[invalid] = self.empty
        result = Integrate1dResult(res.radial, I, sigma)
    else:

        result = Integrate1dResult(res.radial, I)
    result._set_unit(self.unit)
    result._set_sum(sum_)
    result._set_count(count)

    if all:
        logger.warning("integrate1d(all=True) is deprecated. Please refer to the documentation of Integrate2dResult")
        out = {"I": I,
               "radial": res.radial,
               "unit": self.unit,
               "count": count,
               "sum": sum_}
        return out

    return result, results


def integrate(json: str,
              params: Calibration,
              f: Callable[[ndarray], ndarray],
              plot_calibrant: bool=False,
              save: bool=False,
              n: int=10000,
              lst_mask: ndarray=None,
              lst_flat: ndarray=None,
              to_use: bool=False,
              to_plot: bool=True,
              ais: Mapping[int, Text]=None) -> None:
    """Integrate a file with a json calibration file"""
    gonio = pyFAI.goniometer.Goniometer.sload(json)
    print (gonio)
    with File(params.filename, mode='r') as h5file:
        idxs = []
        images = []
        deltas = []
        for frame in gen_metadata_idx(h5file, params, to_use=to_use):
            idxs.append(frame.idx)
            images.append(f(frame.image))    
            deltas.append((frame.delta,))
        if ais is not None:
            all_ais = []
            for i in idxs:
                if i in ais:
                    all_ais.append(ais[i])
                else:
                    all_ais.append(gonio.get_ai(deltas[i]))
            mai = MultiGeometry(all_ais, wavelength=gonio.get_wavelength())
        else:
            mai = gonio.get_mg(deltas)
        res, results = integrate1d(mai, images, n,
                                   lst_mask=lst_mask, lst_flat=lst_flat)
        if save is True:
            try:
                os.makedirs(params.basedir)
            except os.error:
                pass
            numpy.savetxt(os.path.join(params.basedir,
                                       os.path.basename(params.filename) + '.txt'),
                          numpy.vstack([res.radial, res.intensity]).T)
        if to_plot:
            if plot_calibrant:
                calibrant = get_calibrant(params.calibrant, params.wavelength)
                jupyter.plot1d(res, calibrant)
            else:
                jupyter.plot1d(res)
                if results is not []:
                    ax = jupyter.plot1d(results[0])
                    for r in results[1:]:
                        jupyter.plot1d(r, ax=ax)
        return res

def integrate2d_(json: str,
                 params: Calibration,
                 f: Callable[[ndarray], ndarray],
                 plot_calibrant: bool=False,
                 save: bool=False,
                 n: int=10000,
                 lst_mask: ndarray=None,
                 lst_flat: ndarray=None,
                 to_use: bool=False,
                 to_plot: bool=True) -> None:
    """Integrate a file with a json calibration file"""
    gonio = pyFAI.goniometer.Goniometer.sload(json)
    print (gonio)
    with File(params.filename, mode='r') as h5file:
        images = []
        deltas = []
        for frame in gen_metadata_idx(h5file, params, to_use=to_use):
            images.append(f(frame.image))
            deltas.append((frame.delta,))
        mai = gonio.get_mg(deltas)
        res = mai.integrate2d(images, n,
                              lst_mask=lst_mask, lst_flat=lst_flat)
        if save is True:
            try:
                os.makedirs(params.basedir)
            except os.error:
                pass
            numpy.savetxt(os.path.join(params.basedir,
                                       os.path.basename(params.filename) + '.txt'),
                          numpy.vstack([res.radial, res.intensity]).T)
        if to_plot:
            if plot_calibrant:
                calibrant = get_calibrant(params.calibrant, params.wavelength)
                jupyter.plot2d(res, calibrant)
            else:
                jupyter.plot2d(res)
        return res
